# Sms management service

A simple sms management service.

---
The simple sms management service has 2 seperated sections(send and report).
You can choose your sms type such as kavenegar or ghasedak and send sms and get list of them,
Also you can add other type if you want.
---

The project has been doing based on service and repository.
The main classes are App/Base that you can see them such as BaseModel and ...
services typically have generated method and call form controller or other services
also you can call other methods in each service simply.
I used service container for handling some dependency injections in project.
I use Builder and Factory pattern during implementing the project.
The final response as a trait is based on JsonResponse that you can use it somewhere you want.

---
I used **`laravel`** as a main tool to develop code. It's obviously one of the most common frameworks to produce
more efficient and shorter codes. It makes me to focus on business and the logic of project.

This project follows the `RESTFUL` best practices exposing needed endpoints

---

I hope this project will be continued in order to implement other concepts.

---
** Getting started**
---

Launch the application with sail as you can see this package has been added in new version of laravel framework

    $ composer install
    $ sail up -d
    $ sail artisan migrate --seed

    $ sail artisan migrate --seed --env=testing
    $ sail artisan test

### APIs

Method | Path            | Description      |
-------|-----------------|------------------|
POST    | /api/v1/sms | Send Sms         |
GET    | /api/v1/sms     | Retrive Sms list |

Please check it out !

** And also you can see the database tables on [http://localhost:8080)
and Login to phpmyadmin with below information
host=mysql
username=sail
password=password

---

The tools that were used in this project;

- PHP 8.2
- Laravel 10.0
- FormRequest; for validation
- Service Container; for dependency injection
- Sail; for containerization

---
Notice!!!
--
It should be said that Resources for data response,
some features such as pagination and ordering about api lists and some others tools, but
I decided to maintain it as simple as possible, so I did not add them and ignored them for this step.
---
