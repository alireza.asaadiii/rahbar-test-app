<?php

namespace Database\Seeders;

use App\Models\SmsType;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        SmsType::insert([
            ['name' => 'kavehnegar', 'created_at' => now(), 'updated_at' => now()],
            ['name' => 'ghasedak,', 'created_at' => now(), 'updated_at' => now()],
        ]);
    }
}
