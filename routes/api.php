<?php

use Illuminate\Support\Facades\Route;
use App\Http\Middleware\CheckSmsTime;
use App\Http\Controllers\SmsController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::prefix('/v1/sms')->name('auth.')->group( function () {
    Route::post('/', [SmsController::class, 'send'])->middleware(CheckSmsTime::class);;
    Route::get('/', [SmsController::class, 'report']);
});
