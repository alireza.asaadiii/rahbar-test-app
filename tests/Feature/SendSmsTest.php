<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\SmsType;
use Illuminate\Support\Facades\Http;

class SendSmsTest extends TestCase
{
    public function test_send_sms_without_wrong_type(): void
    {
        $data = [
          'phone_number' => '09101777058',
          'type_id' => '3'
        ];

        $response = $this->post('/api/v1/sms', $data);

        $response->assertUnprocessable();
    }

    public function test_send_sms_less_than_five_minutes(): void
    {
        $data = [
            'phone_number' => fake()->numerify('###########'),
            'type_id' => SmsType::KAVEHNEGAR
        ];

        $this->post('/api/v1/sms', $data);
        $response = $this->post('/api/v1/sms', $data);
        $response->assertUnprocessable();
    }

    public function test_send_sms_kavehnegar_status_code(): void
    {
        $data = [
            'phone_number' => fake()->numerify('###########'),
            'type_id' => SmsType::KAVEHNEGAR
        ];

        Http::shouldReceive('async', 'post', 'then')
            ->once()
            ->withAnyArgs()
            ->andReturnSelf();

        $response = $this->post('/api/v1/sms', $data);

        $response->assertNoContent();
    }

    public function test_send_sms_ghasedak_status_code(): void
    {
        $data = [
            'phone_number' => fake()->numerify('###########'),
            'type_id' => SmsType::GHASEDAK
        ];

        Http::shouldReceive('async', 'post', 'then')
            ->once()
            ->withAnyArgs()
            ->andReturnSelf();

        $response = $this->post('/api/v1/sms', $data);

        $response->assertNoContent();
    }

    public function test_send_sms_create_record_in_database(): void
    {
        $data = [
            'phone_number' => fake()->numerify('###########'),
            'type_id' => SmsType::KAVEHNEGAR
        ];

        $this->post('/api/v1/sms', $data);

        $this->assertDatabaseHas('sms_verifications', [
            'type_id' => $data['type_id'],
            'phone_number' => $data['phone_number'],
        ]);
    }
}
