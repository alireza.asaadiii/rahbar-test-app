<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\SmsType;
use App\Services\SmsList\SmsListBuilder;
use App\Services\SmsList\SmsListDirector;

class SmsListTest extends TestCase
{
    public function test_sms_list_status_code(): void
    {
        $response = $this->get('/api/v1/sms');
        $response->assertOk();
    }

    public function test_sms_kavehnegar(): void
    {
        $data = [
          'type_id' => SmsType::KAVEHNEGAR
        ];

        $response = $this->get('/api/v1/sms?type_id=1');
        $response->assertJsonCount((SmsListDirector::build(new SmsListBuilder($data)))->count(), 'data');
    }

    public function test_sms_ghasedak(): void
    {
        $data = [
            'type_id' => SmsType::GHASEDAK
        ];

        $response = $this->get('/api/v1/sms?type_id=2');
        $response->assertJsonCount((SmsListDirector::build(new SmsListBuilder($data)))->count(), 'data');
    }
}
