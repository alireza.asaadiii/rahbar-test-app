<?php

namespace App\Interfaces;

interface SendSmsInterface
{
    public function codeGenerator(): void;
    public function createSmsModel(): void;
    public function send(): void;
}
