<?php

namespace App\Interfaces;

interface SmsListInterface
{
    public function type();
    public function phoneNumber();
    public function get();
}
