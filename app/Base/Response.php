<?php

namespace App\Base;

use \Illuminate\Http\JsonResponse;

Trait Response
{
    public static function success(int $code, string $message, array|object $data = null): JsonResponse
    {
        return response()->json([
            'status' => "Success",
            'code' => $code,
            'message' => $message,
            'data' => $data,
        ], $code);
    }

    public static function error(int $code, string $message, array $errors = []): JsonResponse
    {
        return response()->json([
            'status' => "Error",
            'code' => $code,
            'message' => $message,
            'errors' => $errors
        ], $code);
    }
}
