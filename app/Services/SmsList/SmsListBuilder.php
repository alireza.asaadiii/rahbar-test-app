<?php

namespace App\Services\SmsList;

use App\Models\SmsVerification;
use App\Interfaces\SmsListInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

class SmsListBuilder implements SmsListInterface
{
    private Builder|Model $model;

    public function __construct(public array $data)
    {
        $this->model = new SmsVerification();
    }

    public function type(): void
    {
        if (array_key_exists('type_id', $this->data)) {
            $this->model = $this->model->where('type_id', $this->data['type_id']);
        }
    }

    public function phoneNumber(): void
    {
        if (array_key_exists('phone_number', $this->data)) {
            $this->model = $this->model->where('phone_number', $this->data['phone_number']);
        }
    }

    public function get(): Collection
    {
        return $this->model->get();
    }
}
