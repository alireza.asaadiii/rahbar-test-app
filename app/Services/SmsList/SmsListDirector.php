<?php

namespace App\Services\SmsList;

use App\Interfaces\SmsListInterface;
use Illuminate\Database\Eloquent\Collection;

class SmsListDirector
{
    public static function build(SmsListInterface $smsList): Collection
    {
        $smsList->type();
        $smsList->phoneNumber();

        return $smsList->get();
    }
}
