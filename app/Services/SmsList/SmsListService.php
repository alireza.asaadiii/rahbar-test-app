<?php

namespace App\Services\SmsList;

use Illuminate\Database\Eloquent\Collection;

class SmsListService
{
    public function generate(array $data): Collection
    {
        return SmsListDirector::build(new SmsListBuilder($data));
    }
}
