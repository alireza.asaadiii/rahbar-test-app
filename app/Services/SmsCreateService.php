<?php

namespace App\Services;

use App\Repositories\SmsRepository;
use Illuminate\Support\Facades\App;
use Illuminate\Database\Eloquent\Model;

class SmsCreateService
{
    public function generate(array $data): Model
    {
        return App::make(SmsRepository::class)->create($data);
    }
}
