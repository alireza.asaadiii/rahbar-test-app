<?php

namespace App\Services;

final class CodeGeneratorService
{
    public static function generate(): int
    {
        return fake()->randomNumber(5, true);
    }
}
