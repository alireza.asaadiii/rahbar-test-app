<?php

namespace App\Services\SendSms;

use Illuminate\Support\Facades\DB;
use App\Services\SendSms\Factory\SendSmsFactory;
use App\Services\SendSms\Builder\SendSmsDirector;

class SendSmsService
{
    public function generate(array $data): void
    {
        DB::beginTransaction();
            SendSmsDirector::build(SendSmsFactory::create($data));
        DB::commit();
    }
}
