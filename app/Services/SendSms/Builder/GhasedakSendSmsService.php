<?php

namespace App\Services\SendSms\Builder;

use Exception;
use App\Services\SmsCreateService;
use Illuminate\Support\Facades\Log;
use App\Interfaces\SendSmsInterface;
use Illuminate\Support\Facades\Http;
use App\Services\CodeGeneratorService;
use Symfony\Component\HttpKernel\Exception\ServiceUnavailableHttpException;

class GhasedakSendSmsService implements SendSmsInterface
{
    public function __construct(public array $data){}

    public function codeGenerator(): void
    {
        $this->data['code'] = CodeGeneratorService::generate();
    }

    public function createSmsModel(): void
    {
        (new SmsCreateService)->generate($this->data);
    }

    public function send(): void
    {
        try {
            Http::async()->post('https://api.ghasedak.com/v1/{API-KEY}/sms/send.json
                ?receptor={$this->>data[phoen_number]}&message={$this->data[code]}')
                ->then(function ($response) {
                    echo "Response received!";
                    echo $response->body();
                });
        } catch (Exception $e) {
            Log::error($e);
            throw new ServiceUnavailableHttpException();
        }
    }
}
