<?php

namespace App\Services\SendSms\Builder;

use App\Interfaces\SendSmsInterface;

class SendSmsDirector
{
    public static function build(SendSmsInterface $builder): void
    {
        $builder->codeGenerator();
        $builder->createSmsModel();
        $builder->send();
    }
}
