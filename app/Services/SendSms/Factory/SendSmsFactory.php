<?php

namespace App\Services\SendSms\Factory;

use App\Models\SmsType;
use App\Interfaces\SendSmsInterface;
use App\Services\SendSms\Builder\GhasedakSendSmsService;
use App\Services\SendSms\Builder\KavehNegarSendSmsService;

class SendSmsFactory
{
    public static function create(array $data): SendSmsInterface
    {
        return match ((int)$data['type_id']) {
            SmsType::KAVEHNEGAR =>  new KavehNegarSendSmsService($data),
            SmsType::GHASEDAK =>  new GhasedakSendSmsService($data),
        };
    }
}
