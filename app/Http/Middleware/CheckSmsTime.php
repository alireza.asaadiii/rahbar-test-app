<?php

namespace App\Http\Middleware;

use Closure;
use Exception;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\SmsVerification;
use Illuminate\Http\Response as HttpResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

class CheckSmsTime
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        $lastSms = SmsVerification::where('phone_number', $request->phone_number)
            ->latest()
            ->first();

        if (!$lastSms) {
            return $next($request);
        }

        if ($lastSms) {
            if (Carbon::parse(Carbon::now()->subMinutes(5)) > Carbon::parse($lastSms->created_at) ) {
                return $next($request);
            }
        }

        throw new UnprocessableEntityHttpException();
    }
}
