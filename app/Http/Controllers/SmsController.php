<?php

namespace App\Http\Controllers;

use App\Base\Response;
use App\Http\Requests\SmsCreateRequest;
use App\Services\SendSms\SendSmsService;
use App\Services\SmsList\SmsListService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response as HttpResponse;

class SmsController extends Controller
{
    use Response;

    public function send(SmsCreateRequest $request): JsonResponse
    {
        (new SendSmsService())->generate($request->input());
        return $this->success(
            HttpResponse::HTTP_NO_CONTENT,
            HttpResponse::$statusTexts[HttpResponse::HTTP_NO_CONTENT],
        );
    }

    public function report(Request $request): JsonResponse
    {
        return $this->success(
            HttpResponse::HTTP_OK,
            HttpResponse::$statusTexts[HttpResponse::HTTP_OK],
            (new SmsListService())->generate($request->query())
        );
    }
}
