<?php

namespace App\Models;

use App\Base\BaseModel;

class SmsType extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sms_types';

    CONST KAVEHNEGAR = 1;
    CONST GHASEDAK = 2;
}
