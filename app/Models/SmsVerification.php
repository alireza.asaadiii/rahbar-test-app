<?php

namespace App\Models;

use App\Base\BaseModel;

class SmsVerification extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sms_verifications';
}
