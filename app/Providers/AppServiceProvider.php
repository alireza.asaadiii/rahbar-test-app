<?php

namespace App\Providers;

use App\Models\SmsVerification;
use App\Repositories\SmsRepository;
use Illuminate\Foundation\Application;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->bind(SmsRepository::class, function (Application $app) {
            return new SmsRepository($app->make(SmsVerification::class));
        });
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
